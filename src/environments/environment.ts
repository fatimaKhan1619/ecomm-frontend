// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB9JuijGrW7ikudwKDo2VhGRxVwtkWiUUs',
    authDomain: 'angular-systems.firebaseapp.com',
    databaseURL: 'https://angular-systems.firebaseio.com',
    projectId: 'angular-systems',
    storageBucket: 'angular-systems.appspot.com',
    messagingSenderId: '1068394335973',
    appId: '1:1068394335973:web:aa483225eebcec840d8e0c'
  },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
