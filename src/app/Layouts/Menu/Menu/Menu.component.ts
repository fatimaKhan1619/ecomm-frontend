import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { Router } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { TranslateService } from '@ngx-translate/core';

import { MenuItems } from '../../../Core/menu/menu-items/menu-items';
import { EmbryoService } from 'src/app/Services/Embryo.service';

@Component({
  selector: 'embryo-Menu',
  providers:[EmbryoService],
  templateUrl: './Menu.component.html',
  styleUrls: ['./Menu.component.scss'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({transform: 'rotate(0deg)'})),
      state('expanded', style({transform: 'rotate(180deg)'})),
      transition('expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ])
  ]
})
export class MenuComponent implements OnInit {

   expanded       : boolean;
  public categoryMenu :any
  categoryList: any;
  headerMenuItems: any
   constructor(public menuItems: MenuItems,public router: Router, readonly  embryoService : EmbryoService,
    public translate: TranslateService) {
      this.categoryList= []
      this.headerMenuItems = []
      this.categoryMenu ={}
      this.categoryMenu.state = 'products'
      this.categoryMenu.name = 'CATEGORIES'
      this.categoryMenu.mega = true
      this.categoryMenu.icon = 'part_mode'
   }

   ngOnInit() {
   //  this.getCategoryMenu()
    

   }

   public onItemSelected(item: any) {
      if (item.children && item.children.length) {
         this.expanded = !this.expanded;
      }
   }


   public redirectTo(subchildState){
      this.router.navigate([subchildState.state],{ queryParams:{ category: subchildState.queryState }});
   }
   getCategoryMenu(){
     this.embryoService.getCategories().subscribe(response=>{
      let resp = response as any
      resp.forEach(element => {
        let route = element.payload.doc.data().name.toLowerCase
        element.payload.doc.data().state = `products/${route}`
          this.categoryList.push(element.payload.doc.data())
      });
     this.categoryMenu.children = this.categoryList
     this.headerMenuItems =this.menuItems.getMainMenu()
     this.headerMenuItems.unshift(this.categoryMenu)
    })
   
   }

}
