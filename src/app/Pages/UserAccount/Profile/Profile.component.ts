import { Component, OnInit } from '@angular/core';
import { EmbryoService } from 'src/app/Services/Embryo.service';

@Component({
  selector: 'app-Profile',
  templateUrl: './Profile.component.html',
  styleUrls: ['./Profile.component.scss']
})
export class ProfileComponent implements OnInit {
  model: any;

   constructor(readonly embryoService: EmbryoService) {
     this.model = {}
    }

   ngOnInit() {
     this.embryoService.getCurrentUserInfo().subscribe(response=>{
       this.model = response.length > 0 ? response[0].payload.doc.data() : []
     })
   }

}
