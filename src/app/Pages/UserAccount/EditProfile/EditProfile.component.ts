import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { FormControl, FormGroup, FormBuilder,FormArray, Validators } from '@angular/forms';
import { ToastaService, ToastaConfig, ToastOptions, ToastData} from 'ngx-toasta';
import { AngularFireStorage } from '@angular/fire/storage';
import { EmbryoService } from 'src/app/Services/Embryo.service';
import { storage } from 'firebase';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-EditProfile',
  templateUrl: './EditProfile.component.html',
  styleUrls: ['./EditProfile.component.scss']
})
export class EditProfileComponent implements OnInit {

   type         : string;
   info         : FormGroup;
   address      : FormGroup;
   card         : FormGroup;
   emailPattern : any = /\S+@\S+\.\S+/;
   toastOption  : ToastOptions = {
      title     : "Account Information",
      msg       : "Your account information updated successfully!",
      showClose : true,
      timeout   : 3000,
      theme     : "material"
   };
   model: any;
   id: any;
   uploadPercent: Observable<number>;
   downloadURL: Observable<string>;
   public documents = [];
   constructor(private route: ActivatedRoute,
               private router: Router,
               private formGroup : FormBuilder,
               private toastyService: ToastaService,
               readonly storage: AngularFireStorage,
               readonly embryoService : EmbryoService) {
      this.model = {}

      this.route.params.subscribe(params => {
         this.route.queryParams.forEach(queryParams => {
            this.type = queryParams['type'];
         });   
      });
   }

   ngOnInit() {
      this.info = this.formGroup.group({
         first_name   : ['', [Validators.required]],
         last_name    : ['', [Validators.required]],
         gender       : ['male'],
         date         : [],
         phone_number : ['', [Validators.required]],
         city : ['', [Validators.required]],
         documents: [],
         email        : ['', [Validators.required, Validators.pattern(this.emailPattern)]]
      });

      this.address = this.formGroup.group({
         address      : ['', [Validators.required]],
         buiding_name : ['', [Validators.required]],
         street_no    : ['', [Validators.required]],
         state        : ['', [Validators.required]],
         zip_code     : ['', [Validators.required]],
         country      : ['', [Validators.required]]
      });

      this.card = this.formGroup.group({
         card_number : ['', [Validators.required]],
         cvv         : ['', [Validators.required]],
         name        : ['', [Validators.required]],
         month       : ['', [Validators.required]],
         year        : ['', [Validators.required]]
      })
      this.embryoService.getCurrentUserInfo().subscribe(response=>{
         this.model = response.length > 0 ? response[0].payload.doc.data() : {}
         this.id = response.length > 0 ? response[0].payload.doc.id : ''
        
         if(Object.entries(this.model).length > 0)
         {
            this.info.get('first_name').setValue(this.model.first_name)
            this.info.get('last_name').setValue(this.model.last_name)
            this.info.get('email').setValue(this.model.email)
            this.info.get('date').setValue(this.model.date)
            this.info.get('gender').setValue(this.model.gender)
            this.info.get('phone_number').setValue(this.model.phone_number)
         }
       })
     
      
   }

   /**
    * Function is used to submit the profile info.
    * If form value is valid, redirect to profile page.
    */
   submitProfileInfo() {
      if(this.info.valid){
         this.info.value.date = this.info.value.date ? this.info.value.date.toLocaleString() : 'N/A'
         this.info.controls.documents.setValue(this.documents);
         this.embryoService.updateUser(this.info.value,this.id).then(response=>{
            this.router.navigate(['/account/profile']).then(()=>{
               this.toastyService.success(this.toastOption);
             });
         })
        
      } else {
         for (let i in this.info.controls) {
            this.info.controls[i].markAsTouched();
         }
      }
   }

   /**
    * Function is used to submit the profile address.
    * If form value is valid, redirect to address page.
    */
   submitAddress() {
      if(this.address.valid){
         this.router.navigate(['/account/address']).then(()=>{
           this.toastyService.success(this.toastOption);
         });
      } else {
         for (let i in this.address.controls) {
            this.address.controls[i].markAsTouched();
         }
      }
   }

   /**
    * Function is used to submit the profile card.
    * If form value is valid, redirect to card page.
    */
   submitCard() {
      if(this.card.valid) {
         this.router.navigate(['/account/card']).then(()=>{
           this.toastyService.success(this.toastOption);
         });
      } else {
         for(let i in this.card.controls) {
            this.card.controls[i].markAsTouched();
         }
      }
   }
   uploadFile(event) {
      const file = event.target.files[0];
      const filePath = `${this.model.email}/${file.name}`;
      const fileRef = this.storage.ref(filePath);
      const task = this.storage.upload(filePath, file);
      // observe percentage changes
      this.uploadPercent = task.percentageChanges();
      // get notified when the download URL is available
      task.snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(res => {
            this.documents.push({ documentPath: res, documentName: file.name });
          });
        })
      )
        .subscribe();
    }

}
