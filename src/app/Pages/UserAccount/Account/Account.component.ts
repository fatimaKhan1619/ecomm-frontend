import { Component, OnInit } from '@angular/core';
import { EmbryoService } from 'src/app/Services/Embryo.service';

@Component({
  selector: 'app-Account',
  templateUrl: './Account.component.html',
  styleUrls: ['./Account.component.scss']
})
export class AccountComponent implements OnInit {
model
  constructor(readonly embryoService : EmbryoService) {
    this.model = {}
    this.embryoService.getCurrentUserInfo().subscribe(response=>{
      this.model = response.length > 0 ? response[0].payload.doc.data() : []
    })
   }

  ngOnInit() {
  }

}
