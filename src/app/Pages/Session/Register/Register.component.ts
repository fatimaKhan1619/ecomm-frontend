import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder,FormArray, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { EmbryoService } from 'src/app/Services/Embryo.service';
@Component({
  selector: 'embryo-Register',
  templateUrl: './Register.component.html',
  styleUrls: ['./Register.component.scss']
})
export class RegisterComponent implements OnInit {
  signInForm   : FormGroup;
  emailPattern        : any = /\S+@\S+\.\S+/;
  constructor( private formGroup : FormBuilder , readonly authService : AuthenticationService,
    readonly embryoService : EmbryoService) { 
    this.signInForm = this.formGroup.group({
      user_details       : this.formGroup.group({
         first_name         : ['', [Validators.required]],
         last_name          : ['', [Validators.required]],
         city          : ['', [Validators.required]],
         email              : ['', [Validators.required, Validators.pattern(this.emailPattern)]],
         password        : ['', [Validators.required]],
      })})
  }

  ngOnInit() {
  }
  submit(){
    let userDetailsGroup = <FormGroup>(this.signInForm.controls['user_details']);
    if(userDetailsGroup.valid)
    {
      let model = this.signInForm.value.user_details
      this.embryoService.creatUser(model)
     this.authService.SignUp(model.email , model.password)
      this.embryoService.dbSignUp(model).subscribe(resp=>{
        console.log(resp)
      })
    }else
    {
      for (let i in userDetailsGroup.controls) {
        userDetailsGroup.controls[i].markAsTouched();                                                   
     }
    }
  }

}
