import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { Observable ,  Subscription } from 'rxjs';

import { EmbryoService } from '../../../Services/Embryo.service';

@Component({
  selector: 'app-ProductsList',
  templateUrl: './ProductsList.component.html',
  styleUrls: ['./ProductsList.component.scss']
})
export class ProductsListComponent implements OnInit {

   type          : any;
   pips          : boolean = true;
   tooltips      : boolean = true;
   category      : any;
   pageTitle     : string;
   subPageTitle  : string;
   products : any
   public subscribers: any = {};
   
   constructor(private route: ActivatedRoute,
               private router: Router, 
               public embryoService : EmbryoService,
               ) {
                  this.products = []
   }

   ngOnInit() {
      this.route.params.subscribe(params => {
         this.route.queryParams.forEach(queryParams => {
            this.category = queryParams['category'];
            this.type   = null;
            this.type = params['type'];

            this.getPageTitle();
            this.getProducts()
         });   
      });
   }

   public getPageTitle() {
      this.pageTitle = null;
      this.subPageTitle = null;
      
      switch (this.type || this.category) {
         case undefined:
            this.pageTitle = "Fashion";
            this.subPageTitle="Explore your favourite fashion style.";
            break;

         case "gadgets":
            this.pageTitle = "Gadgets";
            this.subPageTitle="Check out our new gadgets.";
            break;

         case "accessories":
            this.pageTitle = "Accessories";
            this.subPageTitle="Choose the wide range of best accessories.";
            break;
         
         default:
            this.pageTitle = "Products";
            this.subPageTitle = null;
            break;
      }
   }

   public addToCart(value) {
      this.embryoService.addToCart(value);
   }

   public addToWishList(value) {
      this.embryoService.addToWishlist(value);
   }
   
   public getProducts(){
      this.embryoService.getAllProducts(this.type).subscribe(response=>{
         this.products = []
         let resp = response as any
         resp.forEach(element => {
           
            let product = element.payload.doc.data()
            product.name = product.productName
            product.price = product.productPrice
            product.image = product.documents.length > 0 ? product.documents[0].documentPath : ''
            if(this.category)
            product.category && product.category.name === this.category ? this.products.push(product) : ''
            else
            this.products.push(product)
            
         })
      })
   }
}
