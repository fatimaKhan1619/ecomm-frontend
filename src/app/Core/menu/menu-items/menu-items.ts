import { Injectable } from '@angular/core';
import { EmbryoService } from 'src/app/Services/Embryo.service';
import { BehaviorSubject } from 'rxjs';

/*
 * Menu interface
 */
export interface Menu {
  state: string;
  name?: string;
  type?: string;
  icon?: string;
  children?: Menu[];
}

const HeaderOneItems = [
  {
    state: 'home-two',
    name: 'HOME',
    type: 'link',
    icon: 'home'
  },
  // {
  //   state: '',
  //   name: 'SHOP1',
  //   type: 'sub',
  //   icon: 'pages',
  //   children: [
  //     {
  //       state: 'products/men/4',
  //       name: 'PRODUCT DETAILS',
  //       type: 'link',
  //       icon: 'arrow_right_alt'
  //     },
  //     {
  //       state: 'cart',
  //       name: 'CART',
  //       type: 'link',
  //       icon: 'arrow_right_alt'
  //     },
  //     {
  //       state: 'checkout',
  //       name: 'CHECKOUT',
  //       type: 'link',
  //       icon: 'arrow_right_alt'
  //     },
  //     {
  //       state: 'checkout/payment',
  //       name: 'PAYMENT',
  //       type: 'link',
  //       icon: 'arrow_right_alt'
  //     }
  //   ]
  // },


  {
    state: 'products/accessories',
    name: 'ACCESSORIES',
    type: 'link',
    icon: 'party_mode'
  },

  {
    state: 'pages',
    name: 'PAGES',
    type: 'sub',
    icon: 'pages',
    children: [
      {
        state: 'about',
        name: 'ABOUT',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'term-condition',
        name: 'TERM AND CONDITION',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'privacy-policy',
        name: 'PRIVACY POLICY',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'blogs/detail',
        name: 'BLOG DETAIL',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'faq',
        name: 'FAQ',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'not-found',
        name: '404 PAGE',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'account/profile',
        name: 'User Profile',
        type: 'link',
        icon: 'arrow_right_alt',
      }
    ]
  },
  {
    state: 'session',
    name: 'SESSION',
    type: 'sub',
    icon: 'supervised_user_circle',
    children: [
      {
        state: 'session/signin',
        name: 'SIGN IN',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'session/signup',
        name: 'REGISTER',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'session/forgot-password',
        name: 'FORGET PASSWORD',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'session/thank-you',
        name: 'THANK YOU',
        type: 'link',
        icon: 'arrow_right_alt',
      }
    ]
  },
  {
    state: 'contact',
    name: 'CONTACT US',
    type: 'link',
    icon: 'perm_contact_calendar'
  }
];

const FooterOneItems = [
  {
    state: '',
    name: 'ABOUT',
    type: 'sub',
    icon: '',
    children: [
      {
        state: 'about',
        name: 'ABOUT',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'term-condition',
        name: 'TERM AND CONDITION',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'privacy-policy',
        name: 'PRIVACY POLICY',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'faq',
        name: 'FAQ',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'contact',
        name: 'CONTACT US',
        type: 'link',
        icon: 'perm_contact_calendar',
      }
    ]
  },
  {
    state: '',
    name: 'SESSION',
    type: 'sub',
    icon: '',
    children: [
      {
        state: 'session/signin',
        name: 'SIGN IN',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'session/signup',
        name: 'REGISTER',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'session/forgot-password',
        name: 'FORGET PASSWORD',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'session/thank-you',
        name: 'THANK YOU',
        type: 'link',
        icon: 'arrow_right_alt',
      }
    ]
  },
  {
    state: '',
    name: 'CATEGORIES',
    type: 'sub',
    icon: '',
    children: [
      {
        state: 'products/women',
        name: 'WOMEN',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'products/men',
        name: 'MEN',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'products/accesories',
        name: 'ACCESSORIES',
        type: 'link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'products/gadgets',
        name: 'GADGETS',
        type: 'link',
        icon: 'arrow_right_alt',
      }
    ]
  },
  {
    state: '',
    name: 'SOCIAL',
    type: 'sub',
    icon: '',
    children: [
      {
        state: 'https://www.facebook.com/',
        name: 'Facebook',
        type: 'social_link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'https://twitter.com/',
        name: 'Twitter',
        type: 'social_link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'https://www.youtube.com/',
        name: 'Youtube',
        type: 'social_link',
        icon: 'arrow_right_alt',
      },
      {
        state: 'https://plus.google.com',
        name: 'Google Plus',
        type: 'social_link',
        icon: 'arrow_right_alt',
      }
    ]
  }

]

@Injectable()
export class MenuItems {
  categoryList: any;
  categoryMenu: any;
  headerMenuItems: any;
  constructor(readonly embryoService: EmbryoService) {
    this.categoryList = []
    this.categoryMenu = {}
    this.categoryMenu.state = 'products'
    this.categoryMenu.type = 'sub'
    this.categoryMenu.name = 'CATEGORIES'
    this.categoryMenu.mega = true
    this.categoryMenu.icon = 'part_mode'
    this.getCategoryMenu()

  }


  /*
   * Get all header menu
   */
  getCategoryMenu() {
    this.embryoService.getCategories().subscribe(response => {
      this.categoryMenu = {}
      this.categoryList = []
      this.categoryMenu.state = 'products'
      this.categoryMenu.type = 'sub'
      this.categoryMenu.name = 'CATEGORIES'
      this.categoryMenu.mega = true
      this.categoryMenu.icon = 'part_mode'
      let resp = response as any
      resp.forEach(element => {
        let route = element.payload.doc.data().name.toLowerCase()
        let childInfo = element.payload.doc.data()
        childInfo.state = `${route}`
        childInfo.type = 'sub'
        childInfo.icon = 'arrow_right_alt'
        if (childInfo.subCategories) {
          let subChild = childInfo.subCategories
          if (subChild.length > 0) {
            childInfo.children = []
            subChild.forEach(subCaategory => {
              let child: any = {}
              child.state = `products/${route}`
              child.type = 'queryParams'
              child.icon = 'arrow_right_alt'
              child.name = subCaategory.name
              child.queryState = subCaategory.name
              childInfo.children.push(child)
            });
          }
        }


        this.categoryList.push(childInfo)
      });
      this.categoryMenu.children = this.categoryList;
      this.headerMenuItems = [];
      this.headerMenuItems = JSON.parse(JSON.stringify(HeaderOneItems));
      this.headerMenuItems.unshift(this.categoryMenu)
    })

  }
  getMainMenu() {
    return this.headerMenuItems
  }


  /*
   * Get all footer menu
   */
  getFooterOneMenu(): Menu[] {
    return FooterOneItems;
  }
}
