import { Component, OnInit } from '@angular/core';
import { EmbryoService } from 'src/app/Services/Embryo.service';

@Component({
  selector: 'embryo-CtaGroup',
  templateUrl: './CTA-Group.component.html',
  styleUrls: ['./CTA-Group.component.scss']
})
export class CTAGroupComponent implements OnInit {

   callToActionArray : any = []

   constructor(readonly embryoService :EmbryoService) {
      this.callToActionArray = []
    }

   ngOnInit() {
      this.getMainPageProducts()
   }
   getMainPageProducts(){
      this.embryoService.getMainPageProducts().subscribe(response=>{
         this.callToActionArray = response
      })
   }

}
