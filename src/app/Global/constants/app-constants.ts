export const STRING_CONSTANTS = {
    // collection Names
    CATEGORY_COLLECTION : 'Category',
    PRODUCT_COLLECTION:'Product',
    ORDER_DETAILS:'OrderDetails',
    USERS:'Users',
    BANNER_DETAILS:'Banner',
    MAIN_PAGE_PRODUCTS:'MainPageProducts',
}
export const NUMBER_CONSTANTS = {
SHIPPING_COST : 12
}