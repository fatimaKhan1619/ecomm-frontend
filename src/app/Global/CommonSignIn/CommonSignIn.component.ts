import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { EmbryoService } from 'src/app/Services/Embryo.service';

@Component({
  selector: 'embryo-SignIn',
  templateUrl: './CommonSignIn.component.html',
  styleUrls: ['./CommonSignIn.component.scss']
})
export class CommonSignInComponent implements OnInit {
email : string
password : string
  constructor(readonly authService : AuthenticationService,readonly embryoService : EmbryoService) { }

  ngOnInit() {
   
  }
  signIn(){
    this.authService.SignIn(this.email,this.password)
    this.embryoService.dbSignIn(this.email,this.password).subscribe(resp=>{
      console.log(resp)
    })
  }

}
