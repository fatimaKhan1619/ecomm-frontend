import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { EmbryoService } from 'src/app/Services/Embryo.service';

@Component({
  selector: 'embryo-HeaderUserProfileDropdown',
  templateUrl: './HeaderUserProfileDropdown.component.html',
  styleUrls: ['./HeaderUserProfileDropdown.component.scss']
})
export class HeaderUserProfileDropdownComponent implements OnInit {
model
  pofileMenu: boolean;
   constructor(readonly authService : AuthenticationService,readonly embryoService : EmbryoService) { 
     this.model = {}
   }

   ngOnInit() {
    if(this.embryoService.getCurrentUserInfo()){
      this.pofileMenu = true
      this.embryoService.getCurrentUserInfo().subscribe(response=>{
        this.model = response.length > 0 ? response[0].payload.doc.data() : []
      })
    }
    else{
      this.pofileMenu = false
    }
   
   }

   logOut(){
     this.authService.SignOut()

   }
}
